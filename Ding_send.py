# -*- coding: utf-8 -*-
# @Time    : 2023/5/9 0009 10:36
# @Author  : zcj
# @File    : Ding_send.py
# @Software : PyCharm


# 获取jenkins构建信息和本次报告地址

import os
import time
import hmac
import hashlib
import base64
import urllib.parse
import jenkins #安装pip install python-jenkins
import json
import urllib3

from urllib3.exceptions import InsecureRequestWarning
timestamp = str(round(time.time() * 1000))
secret = 'SEC5103f3f646fc7b2010207ff5883da2c2c4eeef437a14b17c44660721f33af26e'
secret_enc = secret.encode('utf-8')
string_to_sign = '{}\n{}'.format(timestamp, secret)
string_to_sign_enc = string_to_sign.encode('utf-8')
hmac_code = hmac.new(secret_enc, string_to_sign_enc, digestmod=hashlib.sha256).digest()
sign = urllib.parse.quote_plus(base64.b64encode(hmac_code))
# print(timestamp)
# print(sign)

url = f'https://oapi.dingtalk.com/robot/send?access_token=64d0f36d9be5c4ab7c210f56ac5f495cc22760e2d7b6f45fa2c90727ce7e43f2&timestamp={timestamp}&sign={sign}'



#利用python发送钉钉  在钉钉需要点击 查看测试报告的话 就需要把报告发送到服务器 再把地址参数传下面  就去掉jenkins所有操作
# jenkins登录地址
jenkins_url = "http://192.167.6.205:8080/jenkins/"
# 获取jenkins对象
server = jenkins.Jenkins(jenkins_url, username='admin', password='admin123') #Jenkins登录名 ，密码
# job名称
job_name = "job/pytest_api/" #Jenkins运行任务名称
# job的url地址
job_url = jenkins_url + job_name
# 获取最后一次构建
job_last_build_url = server.get_info(job_name)['lastBuild']['url']
# 报告地址
report_url = job_last_build_url + 'allure' #'allure'为我的Jenkins全局工具配置中allure别名

'''
钉钉推送方法：
读取report文件中"prometheusData.txt"，循环遍历获取需要的值。
使用钉钉机器人的接口，拼接后推送text
'''

def DingTalkSend():
    d = {}
    # 获取项目绝对路径
    path = os.path.abspath(os.path.dirname((__file__)))
    # 打开prometheusData 获取需要发送的信息
    f = open(path + r'/report/allure-report/export/prometheusData.txt', 'r')
    for lines in f:
        for c in lines:
            launch_name = lines.strip('\n').split(' ')[0]
            num = lines.strip('\n').split(' ')[1]
            d.update({launch_name: num})
    print(d)
    f.close()
    retries_run = d.get('launch_retries_run')  # 运行总数
    print('运行总数:{}'.format(retries_run))
    status_passed = d.get('launch_status_passed')  # 通过数量
    print('通过数量：{}'.format(status_passed))
    status_failed = d.get('launch_status_failed')  # 不通过数量
    print('失败数量：{}'.format(status_failed))
    pass_re = "%.f%%" % (float(status_passed)/float(retries_run)*100)
    print("通过率：{}".format(pass_re))

    # 钉钉推送
    url2 = "https://oapi.dingtalk.com/robot/send?access_token=64d0f36d9be5c4ab7c210f56ac5f495cc22760e2d7b6f45fa2c90727ce7e43f2&timestamp={1684983348335}&sign={jbzQIuXRzqcViC5HA2gC%2BsZtv8b1XoEw3nLRDvr6kaM%3D}"

    url1 = 'https://oapi.dingtalk.com/robot/send?access_token=64d0f36d9be5c4ab7c210f56ac5f495cc22760e2d7b6f45fa2c90727ce7e43f2'  # webhook
    con = {"msgtype": "text",
           "text": {
               "content": "Pytest_Allure_Demo自动化脚本执行完成。"
                          "\n测试概述:"
                          "\n运行总数:" + retries_run +
                          "\n通过数量:" + status_passed +
                          "\n失败数量:" + status_failed +
                          "\n通过率:" + pass_re +
                          "\n构建地址：\n" + job_url +
                          "\n报告地址：\n" + report_url
           },
           "at": {
               "atMobiles": [
                   "130xxxxxxxx"  # 如果需要@某人，这里写他的手机号
               ],
               "isAtAll": 1  # 如果需要@所有人，这些写1
           }

           }
    try:
        urllib3.disable_warnings()
        http = urllib3.PoolManager()
        jd = json.dumps(con)
        jd = bytes(jd, 'utf-8')
        http.request('POST', url, body=jd, headers={'Content-Type': 'application/json'})

    except Exception as e:
        print(e)



if __name__ == '__main__':
    DingTalkSend()


