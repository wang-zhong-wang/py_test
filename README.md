# py_test
 介绍

 基于pytest+request+yaml+allure实现的接口自动化测试框架，按照指定格式编写yaml文件可自动转为测试用例py文件执行，用例关键字extract提取响应数据保存到extract.yaml文件供其他接口调用。

环境准备
建议使用python3.8及以上版本解释器

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

### 目录架构及简要概述

![py文件信息](conf/image.png)

### 使用说明

pytest.ini文件中定义了扫描的路径，testpaths，可以自行修改

在case目录下编写test开头且**.yaml/.yml**结尾的文件，可再建模块目录，将该模块的接口归类



### yaml用例编写格式
![用例编写](conf/case.png)

### 配置文件编写格式

![配置文件](conf/conf.png)

### 执行日志效果

![输入图片说明](conf/console.png)

### allure报告展示

![输入图片说明](conf/report.png)
![输入图片说明](images/demo.png)

### 结合jenkins进行测试结果 钉钉推送


![输入图片说明](images/d_resu.png)

### 参与贡献



1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


### 特技


1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
