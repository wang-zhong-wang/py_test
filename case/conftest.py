import pytest
from common.util import get_resp
from common.util import data_path
from common.read_yaml import clear_yaml, get_yaml_data
from common.request_0 import HttpRequest
session = HttpRequest()

data = get_yaml_data(data_path+'/login_token.yaml')['login'][0]
url = data['request']['url']
method = data['request']['method']
token = data['extract_data']
da = data['json']

@pytest.fixture(scope="session",autouse=True)#默认是函数级别  如果不添加 session  则会在每条用例都执行一次 前置和后置操作
def excute_data():
    print('请求的前置')
    re = session.request(method,url,da)
    get_resp(re, token)   #提取tonken数据

    yield
    clear_yaml()
    print("请求后置------清理关键字数据")



