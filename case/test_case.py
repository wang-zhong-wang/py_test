import pytest
import requests
import allure
from common.util import assert_response, get_resp, change_data, data_path
from common.debug import de_bug,ass
from common.read_yaml import write_yaml
from common.read_yaml import get_yaml_data
from common.request_0 import HttpRequest
from common.logget import logger
from common import read_config

case_filename=read_config.Docof().get_option("execute_case")[0]+".yaml"  #用例文件名
case_option=read_config.Docof().get_value_from_option("execute_case","test_case")#用例下的选项 节点
data = get_yaml_data(data_path + f'/{case_filename}')[case_option]  # 读取节点下全部数据
session = HttpRequest()  # 实列化 封装好的request

@pytest.mark.parametrize('info', data)

def test_all(info):
    apiname = info['title']
    url = info['request']['url']
    method = info['request']['method'] #如果方法是delete 则提取删除的id 拼接到url
    header = info['headers']
    json = info['json']

    # 更新鉴权码

    if method == 'delete':
        url=change_data(info)
    #接口关联
    change_data(info)  # 判断是否需要提取extract.yaml文件中的数据 做关联,并代入请求体，如果提取失败会告诉具体的apiname
    allure.dynamic.title(info['title'])  # 测试数据 请求标题

    with allure.step(f'1.接口地址： {url}'): #步骤下面必须要有数据  来判断此步骤是否执行成功，显示在allure报告中
        url = url

    with allure.step(f'2.请求数据： {json}'):
        json = info['json']

    with allure.step(f'3.发送{method}请求'):
        re = session.request(method, url, json, headers=header, allow_redirects=False)
    de_bug(title=apiname, url=url, method=method, info=info, respond=re)  # 打印操作的数据日志  写在下面的话如果提取响应失败会抛出异常 则就不会执行了
    get_resp(re, info['response_extraction'])  # 提取响应数据 写入extract.yaml文件，如果用例此字段为空的话就不需要提取响应数据
    # allure.attach(info,'请求',allure.attachment_type.TEXT)
    with allure.step(f"4.断言：期望值={info['expected']}"):
        assert_response(re, info['expected'])
        ass()   #上面步骤没啥异常  就说明测试通过 打印日志，有异常则不会进行到这步




# if __name__ == '__main__':
#     # pytest.main([__file__, '-v'])
#  print(data)
