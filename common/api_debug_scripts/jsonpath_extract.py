#coding=utf-8
import json
import jsonpath

from common.read_yaml import write_yaml
a = [{'rex': ["$..['rows'][0][id]", 'sta']},{'rex': ['$.code', 'cod']}]
response={
    "code": 200,
    "satus": 200,
    "message": "请求成功",
    "data": {
        "pageNum": 1,
        "pageSize": 20,
        "total": 16,
        "pages": 1,
        "rows": [
            {
                "id": "25796120-c71e-4dc0-940f-f20e03d4e3bc",
                "name": "777777",

            },
            {
                "id": "332c09f5-2107-416d-82dc-f97e95b9eba4",
                "name": "科技园危化公司",


            },

            {
                "id": "eca7fda0-70c1-4912-86e8-9c09638c4f91",
                "name": "而我却恶趣味",
            },
            {
                "id": "f082c99f-f144-42a9-8eb8-462107896d6b",
                "name": "鹅鹅鹅鹅鹅鹅饿我",

            }
        ]
    }
}
def get_resp(response, extract_key):
    # 将字符串作为输入并返回字典作为输出。
    # text = json.loads(res)
    # 将字符串作为输入并返回字典作为输出。
    # text1 = json.dumps(res)
    ty_pe = type(response)
    if ty_pe is str:
        text = json.loads(response)
    else:
        text = response

    if extract_key:

        for i in extract_key:
            if "rex" in i.keys():
                yaml_result = i.get("rex")[0]
                # actual_result = jsonpath.jsonpath(response.json(),f'$..{yaml_result}')#去掉$..是考虑返回的json数据如果存在两个一样的id
                extract_value = jsonpath.jsonpath(text.json(), f'{yaml_result}') #jsonpath提取的值  取到的值在列表里 如 [200]

                input_key = i.get("rex")[1]  #自定义的键
                print(input_key)
                new_dict = {input_key: extract_value[0]}  #组合新的字典写入到 关键字 文件中

                write_yaml(new_dict) #写入yaml文件


    else:
        print("此请求不需要提取响应参数")




if __name__ == '__main__':
    get_resp(response,a)
