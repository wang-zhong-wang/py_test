#coding=utf-8
import json

import jsonpath

a ={
    "code": 200,
    "satus": 200,
    "message": "请求成功",
    "data": {
        "pageNum": 1,
        "pageSize": 20,
        "total": 16,
        "pages": 1,
        "rows": [
            {
                "id": "25796120-c71e-4dc0-940f-f20e03d4e3bc",
                "name": "777777",

            },
            {
                "id": "332c09f5-2107-416d-82dc-f97e95b9eba4",
                "name": "科技园危化公司",


            },

            {
                "id": "eca7fda0-70c1-4912-86e8-9c09638c4f91",
                "name": "而我却恶趣味",
            },
            {
                "id": "f082c99f-f144-42a9-8eb8-462107896d6b",
                "name": "鹅鹅鹅鹅鹅鹅饿我",

            }
        ]
    }
}

def get_text(res,key):
    # 将字符串作为输入并返回字典作为输出。
    # text = json.loads(res)
    #将字符串作为输入并返回字典作为输出。
    # text1 = json.dumps(res)
    ty_pe = type(res)
    if ty_pe is str:
        text = json.loads(res)
    else:
        text =res
    #jsonpath就相当于ui自动化中的xpath元素定位
    value = jsonpath.jsonpath(text,f'{key}')
    if value:
        if len(value) == 1:
            return value[0]

    else:
        return None
if __name__ == '__main__':
    print( get_text(a,"$..['rows'][0][id]"))