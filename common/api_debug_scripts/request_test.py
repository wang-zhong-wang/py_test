# -*- coding: utf-8 -*-
# @Time    : 2023/4/24 0024 15:46
# @Author  : zcj
# @File    : request_test.py
# @Software : PyCharm
import requests

data={"id":"028afe11-e40f-4ec3-8157-a176df5392be"}
url="http://192.167.6.185:8090/dah-park-api/system/book/"+data["id"]   #针对删除的接口
head={"Accept": "application/json, text/plain, */*",
"Accept-Encoding": "gzip, deflate",
"Accept-Language": "zh-CN,zh;q=0.9",
"Authorization": "YWNjM2VhNmEtYzA4OS00ZmZkLThjNjYtYzViNjQ2MGI4Y2QxLjRiZmZiYjMxLTU5"
}


re=requests.delete(url=url,headers=head)
print(re.json())
