# -*- coding: utf-8 -*-
# @Time    : 2023/6/1 0001 13:40
# @Author  : zcj
# @File    : db.py
# @Software : PyCharm
import pymysql


class DB:
    def __init__(self):
        self.conn = pymysql.connect(host='192.167.6.181',
                    port=3306,
                    user='root',
                    passwd='Test123456',   # passwd 不是 password
                    db='dah-park-cds')
        self.cur = self.conn.cursor()

    def __del__(self): # 析构函数，实例删除时触发
        self.cur.close()
        self.conn.close()

    def query(self, sql):
        self.cur.execute(sql)
        return self.cur.fetchall()

    def exec(self, sql):
        try:
            self.cur.execute(sql)
            self.conn.commit()
        except Exception as e:
            self.conn.rollback()
            print(str(e))

    def check_user(self,name):
        result = self.query("select * from user where name='{}'".format(name))
        return True if result else False

    def del_user(self, name):
        self.exec("delete from user where name='{}'".format(name))


if __name__ == '__main__':
    d=DB()
    a=d.query("SELECT * FROM system_book WHERE name='1'")
    print(len(a))