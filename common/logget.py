import logging

logger = logging.getLogger('apilog')
logger.setLevel(logging.DEBUG)

format = logging.Formatter("%(asctime)s|%(levelname)8s|%(filename)5s|:%(lineno)s|%(message)s")
fl = logging.FileHandler(filename=r"logs/api.log", mode='a',encoding='utf-8') #输出文件

fl.setFormatter(format) #输出格式

sl = logging.StreamHandler()
sl.setFormatter(format)

logger.addHandler(fl)  #输出到api.log文件
logger.addHandler(sl)  #输出到控制台
