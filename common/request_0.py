import requests


class HttpRequest:
    """
把get和post封装成为一个类
    """

    def __init__(self):
        self.session = requests.session() #创建一个session会话保持cookies

    #位置传参 此代码不用写data=None,做自动化 根据method自动判断用par还是data,如果加上data=None 那么在写代码的时候传参，还要做 是否是post的判断，然后写data=｛xxx｝进行关键字传参。
    #因为是位置参数 所以直接传字典的话 他只会传给params
    def request(self, method, url, params=None,data=None, json=None,headers=None,**kwargs):
        if method.upper() == 'GET':
            return self.session.request(method,url,params=params,headers=headers,**kwargs)
        # 减少后面代码层面的判断，在请求时提前判断
        elif method == 'POST':#大写post是用data传参
            return self.session.request(method,url,data=params,headers=headers,**kwargs)
        elif method == 'post':  #小写post是用json传参
            return self.session.request(method, url, json=params, headers=headers, **kwargs)
        elif method.lower() == 'delete':
            return self.session.request(method,url,headers=headers,**kwargs)

        else:
            print('请使用get或者post')

    def close_session(self):
        self.session.close()

# if __name__ == '__main__':
#     session = HttpRequest()
#     session.request("get","sfsd")