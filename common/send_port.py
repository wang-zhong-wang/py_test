#coding: utf-8
import os
import time
from common import smtp_email_send
from common import read_config
send_email=read_config.Docof().get_value_from_option('send_email', 'send_or_not') #读取配置文件 判断是否需要发送报告



def send_port():
    if send_email in ["send", 'y', 'Y', "yes"]:
        print("报告发送中...")
        if os.path.exists('report\\send_dir.zip'):
            os.system('del -s -q report\\send_dir.zip')
            print("成功删除report目录下 已存在的send_dir.zip")
        if os.path.exists('report\\send_dir\\allure-report'):
            os.system(r'rmdir /s /q report\\send_dir\\allure-report')
            print("成功删除send_dir目录下 已存在的allure-report")

            # # 移动文件到send_dir里面，allure-2.9.0 已经在了send_dir 不能删除
        os.system('move report/allure-report report/send_dir')

        b = smtp_email_send.SendEmail('report\\send_dir.zip', r'report\\send_dir', r"report\\send_dir.zip")
        # 压缩文件夹
        b.zipDir()
        time.sleep(10)
        # 默认发送当前文件夹下的allure-report.zip
        # 调取发送邮件接口，传入文件参数
        b.run()


    else:
        print("本次----未发送报告")


if __name__ == '__main__':
   print(send_email)


