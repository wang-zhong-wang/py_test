# -*- coding: utf-8 -*-
# @Time    : 2023/5/26 0026 15:39
# @Author  : zcj
# @File    : single_convert.py
# @Software : PyCharm
from common.util import data_path

from common.read_yaml import get_yaml_data


ta = get_yaml_data(data_path + f'/test_single.yaml')["one"]  # 读取节点下全部数据

data = [{'id': 1, 'request': {'url': 'http://192.167.6.185:8090/dah-park-api/unit-document/page', 'method': 'GET'},
         'headers': {'Accept': 'application/json, text/plain, */*', 'Accept-Encoding': 'gzip, deflate', 'Accept-Language': 'zh-CN,zh;q=0.9'},
         'relation': None,
         'parametrize': [{'test_tit': '错误的入参', 'json': {'pageNum': 1, 'pageSize': 20}, 'ex': [{'eq': ['$.code', 200]}]},
                         {'test_tit': '错误的数据', 'json': {'pageNum': 1, 'pageSize': 20}, 'ex': [{'eq': ['$.code', 200]}]},
                         {'test_tit': None, 'json': {'pageNum': 1, 'pageSize': 20}, 'ex': [{'eq': ['$.code', 200]}]}]}]




def single_data_convert(data):
    info = ta[0]["parametrize"]
    d = []
    for i in info:  #有几个迭代对象，就循环几次  每次向字典 添加元素

        i["request"] = data[0]["request"]
        i["headers"] = data[0]["headers"]
        i["relation"] = data[0]["relation"]
        d.append(i)
    return d
print(single_data_convert(ta))