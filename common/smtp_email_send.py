# -*-coding:GBK -*-
import os
import smtplib
import zipfile
from email.mime.text import MIMEText  # 正文用
from email.header import Header  # 标题用
from email.utils import formataddr
from common import read_config

class SendEmail:

    def __init__(self, fujian_name, dirpath, outFullName):
        self.fujian_name = fujian_name #待发送文件的路径
        self.dirpath = dirpath
        self.outFullName = outFullName #待压缩文件的路径
        self.user = read_config.Docof().get_value_from_option('Email', 'send_User')
        self.pwd = read_config.Docof().get_value_from_option('Email', 'send_Password')
        self.st = read_config.Docof().get_value_from_option('Email', 'send_Smtp')

        self.addr =read_config.Docof().get_option("addreSsee")  #获取节点下的 option（key）以列表返回    因为run方法下的addreSsee 要用列表,发送多人邮箱


    def zipDir(self):
        zip = zipfile.ZipFile(self.outFullName, "w", zipfile.ZIP_DEFLATED)
        for path, dirnames, filenames in os.walk(self.dirpath):
            # 去掉目标跟路径，只对目标文件夹下边的文件及文件夹进行压缩
            fpath = path.replace(self.dirpath, '')


            for filename in filenames:
                zip.write(os.path.join(path, filename), os.path.join(fpath, filename))
        zip.close()
        print('压缩成功')

    def run(self):

        send_User = self.user  # 邮箱地址
        send_Password = self.pwd  # 163邮箱的smtp密码  我用qq
        send_Smtp = self.st  # 使用的邮箱smtp官方地址
        addreSsee = self.addr  # 收件箱地址,列表  可以配置多个邮箱 addreSsee = ['4956801@qq.com', '24171021@qq.com', '14201045@qq.com']
        # 发送的附件内容
        # msg = MIMEText('hh', 'base64', 'utf-8')  # 数据编码方式是utf-8
        msg = MIMEText(open(self.fujian_name, 'rb').read(), "base64", "utf-8")
        msg['Content-Type'] = 'application/octet-stream'
        msg["Content-Disposition"] = 'attachment; filename=' + self.fujian_name

        # 第一种发件人方式：
        msg['From'] = send_User  # 发件人，声明收到邮件的时候发件人显示用（11不能写中文）
        print(type(msg['From']))  # 是一个email的对象
        msg['Subject'] = Header(u'testapi的邮件', 'utf-8').encode()  # u一般用在中文字符串前面，防止因为源码储存格式问题
        msg['To'] = u'人员'  # 收件人的名字（随便取）

        # print(msg)  # 打印发送给接受的数据
        # print(type(msg))
        # 实例化一个邮箱
        smtp = smtplib.SMTP()
        smtp.connect(send_Smtp, 25)  # 这是发送出去的邮箱的smtp的地址和默认端口
        smtp.login(send_User, send_Password)  # 这是登录的账号和smtp密码，用于除了网页以外的客户端登录
        # 发送邮件，传入发送账号，接收账号，将from和to subject当做字符串传入
        try:
            smtp.sendmail(send_User, addreSsee, msg.as_string())
            smtp.quit()  # 关闭连接
            print("邮箱发送成功")
        except Exception as e:
            print(f"发送失败-{e}")

# 调试尝试一下是否调通
# a = SendEmail('test_01.py',r'send_dir',r"send_dir.zip")
# a.run()
# a.zipDir()

# if __name__ == '__main__':
#     a=read_config.Docof().get_value_from_option('Email', 'addreSsee')
#
#     print([a])
